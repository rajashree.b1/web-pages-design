/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _styles_globals_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../styles/globals.scss */ \"./styles/globals.scss\");\n/* harmony import */ var _styles_globals_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_styles_globals_scss__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _styles_colors_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/_colors.scss */ \"./styles/_colors.scss\");\n/* harmony import */ var _styles_colors_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_colors_scss__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _styles_mixins_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/_mixins.scss */ \"./styles/_mixins.scss\");\n/* harmony import */ var _styles_mixins_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_mixins_scss__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _styles_variables_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../styles/_variables.scss */ \"./styles/_variables.scss\");\n/* harmony import */ var _styles_variables_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_variables_scss__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _styles_iconfonts_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../styles/iconfonts.css */ \"./styles/iconfonts.css\");\n/* harmony import */ var _styles_iconfonts_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_styles_iconfonts_css__WEBPACK_IMPORTED_MODULE_5__);\n\n\n\n\n\n\n// import \"../components/BtnComponent.js\";\n// import \"../js/custom.js\";\nfunction MyApp({ Component , pageProps  }) {\n    return /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, {\n        ...pageProps\n    }, void 0, false, {\n        fileName: \"/Users/rajashree/ws/Development/NextJs/web-pages-design/pages/_app.js\",\n        lineNumber: 11,\n        columnNumber: 5\n    }, this);\n// <div className='hello'>Hello World</div>\n}\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MyApp);\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9fYXBwLmpzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUErQjtBQUNDO0FBQ0E7QUFDRztBQUNGO0FBQ2pDLDBDQUEwQztBQUMxQyw0QkFBNEI7QUFFNUIsU0FBU0EsS0FBSyxDQUFDLEVBQUVDLFNBQVMsR0FBRUMsU0FBUyxHQUFFLEVBQUU7SUFDdkMscUJBQ0UsOERBQUNELFNBQVM7UUFBRSxHQUFHQyxTQUFTOzs7OztZQUFJLENBQzdCO0FBQ0QsMkNBQTJDO0NBQzVDO0FBRUQsaUVBQWVGLEtBQUssRUFBQyIsInNvdXJjZXMiOlsid2VicGFjazovL215LWFwcC8uL3BhZ2VzL19hcHAuanM/ZTBhZCJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgJy4uL3N0eWxlcy9nbG9iYWxzLnNjc3MnXG5pbXBvcnQgXCIuLi9zdHlsZXMvX2NvbG9ycy5zY3NzXCI7XG5pbXBvcnQgXCIuLi9zdHlsZXMvX21peGlucy5zY3NzXCI7XG5pbXBvcnQgXCIuLi9zdHlsZXMvX3ZhcmlhYmxlcy5zY3NzXCI7XG5pbXBvcnQgXCIuLi9zdHlsZXMvaWNvbmZvbnRzLmNzc1wiO1xuLy8gaW1wb3J0IFwiLi4vY29tcG9uZW50cy9CdG5Db21wb25lbnQuanNcIjtcbi8vIGltcG9ydCBcIi4uL2pzL2N1c3RvbS5qc1wiO1xuXG5mdW5jdGlvbiBNeUFwcCh7IENvbXBvbmVudCwgcGFnZVByb3BzIH0pIHtcbiAgcmV0dXJuIChcbiAgICA8Q29tcG9uZW50IHsuLi5wYWdlUHJvcHN9IC8+XG4gIClcbiAgLy8gPGRpdiBjbGFzc05hbWU9J2hlbGxvJz5IZWxsbyBXb3JsZDwvZGl2PlxufVxuXG5leHBvcnQgZGVmYXVsdCBNeUFwcDsiXSwibmFtZXMiOlsiTXlBcHAiLCJDb21wb25lbnQiLCJwYWdlUHJvcHMiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./pages/_app.js\n");

/***/ }),

/***/ "./styles/_colors.scss":
/*!*****************************!*\
  !*** ./styles/_colors.scss ***!
  \*****************************/
/***/ (() => {



/***/ }),

/***/ "./styles/_mixins.scss":
/*!*****************************!*\
  !*** ./styles/_mixins.scss ***!
  \*****************************/
/***/ (() => {



/***/ }),

/***/ "./styles/_variables.scss":
/*!********************************!*\
  !*** ./styles/_variables.scss ***!
  \********************************/
/***/ (() => {



/***/ }),

/***/ "./styles/globals.scss":
/*!*****************************!*\
  !*** ./styles/globals.scss ***!
  \*****************************/
/***/ (() => {



/***/ }),

/***/ "./styles/iconfonts.css":
/*!******************************!*\
  !*** ./styles/iconfonts.css ***!
  \******************************/
/***/ (() => {



/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/_app.js"));
module.exports = __webpack_exports__;

})();