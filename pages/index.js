import Head from 'next/head'
// import Image from 'next/image'
import Navbar from '../components/Navbar';
// import styles from '../styles/Home.module.css'

export default function Index() {
  return (
    <div>
      <Head>
        <title>Home App</title>
        {/* <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin /> */}
        {/* Baloo 2 font link */}
        {/* <link href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@400;500;600;700;800&display=swap" rel="stylesheet" />  */}

        {/* Roboto font link */}
        {/* <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin /> */}
        {/* <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet" /> */}

      </Head>
      <header>
        <Navbar></Navbar>
      </header>
    
      <div className="c_container">
        <Head>
          <title>Create Next App</title>
          <meta name="description" content="Generated by create next app" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
      </div>
    </div>
  )
}
