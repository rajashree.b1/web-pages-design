// import Navbar from "../components/Navbar";
// import { SassColor } from "sass";

import styles from "../styles/Login.module.scss";
import Image from 'next/image';
// import MyImg from "../public/logo";

export default function LoginEmail_ResetPass(){
  return(
    <div>
        <div className={styles.login_wrapper}>

            {/* login header */}
            <header className={styles.logo}>
                <a href="https://www.jumbaya.com/" target="_blank">
                    
                    <Image
                        src={"/logo.svg"}
                        alt="Logo"
                        width={"100%"}
                        height={61}
                    />
                </a>
            </header>

            {/* main content section */}
            <form role="form" id={styles.FormWrapper} className={styles.FormWrapper}>
                <div className={styles.Form__content}>
                <h1 className="text_center h_small__reg Primary_4 mt_40 mb_16">Reset your password</h1>
                    <div className={` ${styles.Form__EmailSection} Bgwhite box_shadow1`}>
                        <p className="mt_0 body_small__semibold text_3">Your registered email ID</p>

                        <div className={styles.Form__EnterEmail}>
                            <input className={` ${styles.edit_email} Primary_4`} value="johndoe@gmail.com" readOnly/>
                        </div>

                        <div className={` ${styles.Form__EnterPass} mt_24 mb_24`}>
                            <p className="mt_0 body_small__semibold text_3">Create New Password</p>

                            <input type="password" id="pwd" name="password" required></input>
                            <i className="fa-solid fa-eye" id="togglePWD"></i>
                            <i className="fa-solid fa-eye-slash hide" id="togglePWD"></i>
                        </div>

                        <button type="button" className={` ${styles.submit_btn} btn_Exlarge b_label__medium white`}>Submit</button>
                        <a href="#" className={` ${styles.forgot_pwd} text_center`}>forgot password</a>
                    </div>

                    <div className={styles.signUp_link}>
                        <span className="body_small__reg Black">Don’t have an account?</span>
                        <a href="#" className="b_label__bold Primary_4">sign up</a>
                    </div>
                </div>
            </form>

            {/* footer section */}
            <footer className={styles.footerSection}>
                <ul>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Privacy Policy</a>
                    </li>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Terms of Service</a>
                    </li>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Help</a>
                    </li>
                </ul>
                <a href="#" className="text_center label_small__reg text_3">Google privacy policy and terms of service</a>
            </footer>
        </div>
    </div>
  )
}
  