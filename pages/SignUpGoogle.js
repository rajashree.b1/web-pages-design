// import Navbar from "../components/Navbar";
// import { SassColor } from "sass";

import styles from "../styles/Login.module.scss";
import Image from 'next/image';
// import MyImg from "../public/logo";

export default function SignUpGoogle(){
  return(
    <div>
        
        <div className={styles.login_wrapper}>

            {/* login header */}
            <header className={styles.logo}>
                <a href="https://www.jumbaya.com/" target="_blank">
                    
                    <Image
                        src={"/logo.svg"}
                        alt="Logo"
                        width={"100%"}
                        height={61}
                    />
                </a>
            </header>

            {/* main content section */}
            <form role="form" id={styles.FormWrapper} className={styles.FormWrapper}>
                <div className={styles.Form__content}>
                    <div className={` ${styles.googleForm} mt_40 mb_16`}>
                        <div className={styles.googleForm__header}>
                            <h4 className="m_0 body_small__reg">
                            <Image
                                src={"/google-logo.svg"}
                                alt="Google Logo"
                                width={14}
                                height={14}
                            />
                            <span>Sign Up with Google</span>
                            </h4>
                        </div>
                        <div className={styles.googleForm__content}>
                            <div className={styles.googleForm__compLogo}>
                                <Image
                                    src={"/company-logo.svg"}
                                    alt="Company Logo"
                                    width={37}
                                    height={37}
                                    className="text_center"
                                />
                            </div>
                            
                            <h5 className="h_small__reg mt_16 mb_24 text_center">Choose an account
                                <p className="m_0">to continue to <span>Jumbaya</span></p>
                            </h5>

                            <div className={styles.googleForm__AccountsWrapper}>
                                <ul className="m_0">
                                    <li role="link">
                                        <div className={styles.googleForm__AccountsDetails}>
                                            <div className={styles.googleForm__AccountsImgs}>
                                                <Image
                                                    src={"/profile-picture.svg"}
                                                    alt="Profile Picture"
                                                    width={28}
                                                    height={28}
                                                />
                                            </div>
                                            <div className={styles.googleForm__AccountsName}>
                                                <h6 className="m_0">John Doe</h6>
                                                <p className="m_0">johndoe@gmail.com</p>
                                            </div>
                                        </div>
                                    </li>

                                    <li role="link">
                                        <div className={styles.googleForm__AccountsDetails}>
                                            <div className={styles.googleForm__AccountsImgs}>
                                                <Image
                                                    src={"/profile-picture.svg"}
                                                    alt="Profile Picture"
                                                    width={28}
                                                    height={28}
                                                />
                                            </div>
                                            <div className={styles.googleForm__AccountsName}>
                                                <h6 className="m_0">Account Name</h6>
                                                <p className="m_0">email@gmail.com</p>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div className={styles.googleForm__AccountsDetails}>
                                            <div className={styles.googleForm__AccountsImgs}>
                                                <Image
                                                    src={"/account-icon.svg"}
                                                    alt="Account Icon"
                                                    width={20}
                                                    height={20}
                                                />
                                            </div>
                                            <div className={styles.googleForm__AccountsName}>
                                                <h6 className="m_0">Use another account</h6>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div className={styles.googleForm__AccountsDescp}>
                                <p>To continue, Google will share your name, email address, language preference, and profile picture with Company. Before using this app, you can review Company’s <br></br>
                                <a href="#">privacy policy</a> and <a href="#">terms of service.</a></p>
                            </div>
                        </div>
                    </div>

                    <div className={styles.signUp_link}>
                        <span className="body_small__reg Black">Already have an account?</span>
                        <a href="#" className="b_label__bold Primary_4">login</a>
                    </div>
                </div>
            </form>

             {/* footer section */}
            <footer className={styles.footerSection}>
                <ul>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Privacy Policy</a>
                    </li>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Terms of Service</a>
                    </li>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Help</a>
                    </li>
                </ul>
                <a href="#" className="text_center label_small__reg text_3">Google privacy policy and terms of service</a>
            </footer>
        </div>
    </div>
  )
}
  