// import Navbar from "../components/Navbar";
// import { SassColor } from "sass";

import styles from "../styles/Login.module.scss";
import Image from 'next/image';
// import MyImg from "../public/logo";

export default function SignUp_AccComplt(){
  return(
    <div>
        
        <div className={styles.login_wrapper}>

            {/* login header */}
            <header className={styles.logo}>
                <a href="https://www.jumbaya.com/" target="_blank">
                    
                    <Image
                        src={"/logo.svg"}
                        alt="Logo"
                        width={"100%"}
                        height={61}
                    />
                </a>
            </header>

            {/* main content section */}
            <form role="form" id={styles.FormWrapper} className={styles.FormWrapper}>
                <div className={styles.Form__content}>
                    <h1 className="text_center h_large__reg Primary_4 mt_40 mb_16 pl_16 pr_16">Account creation complete!
                        <p className="body_small__reg text_3">Let’s jump right in!</p>
                        <Image
                            src={"/acc-compl-icon.svg"}
                            alt="Account Complete"
                            width={100}
                            height={100}
                        />
                    </h1>
                </div>
            </form>

            {/* footer section */}
            <footer className={styles.footerSection}>
                <ul>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Privacy Policy</a>
                    </li>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Terms of Service</a>
                    </li>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Help</a>
                    </li>
                </ul>
                <a href="#" className="text_center label_small__reg text_3">Google privacy policy and terms of service</a>
            </footer>
        </div>
    </div>
  )
}
  