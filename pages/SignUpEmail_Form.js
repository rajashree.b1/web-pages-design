import styles from "../styles/Login.module.scss";
import Image from 'next/image';

export default function SignUpEmail_Form(){
  return(
    <div>
        
        <div className={styles.login_wrapper}>

            {/* login header */}
            <header className={styles.logo}>
                <a href="https://www.jumbaya.com/" target="_blank">
                    
                    <Image
                        src={"/logo.svg"}
                        alt="Logo"
                        width={"100%"}
                        height={61}
                    />
                </a>
            </header>

            {/* main content section */}
            <form role="form" id={styles.FormWrapper} className={styles.FormWrapper}>
                <div className={styles.Form__content}>
                    <div className={` ${styles.signup_Form} mt_40 mb_16`}>
                        <div className={` ${styles.signup_Form__content} Bgwhite  box_shadow1`}>
                            
                            <h5 className="h_large__reg Primary_4 m_0 text_center p_50">Great! You’re almost done</h5>
                            <p className="body_small__reg text_3 text_center mt_0">Let’s just confirm some details to finish your account creation!</p>

                            <div className={` ${styles.signup_Form__EnterName} mb_24`}>
                                <p className="m_0 body_small__semibold text_3">What is your name?
                                    <span className="label_small__reg Primary_4">We’ll address you by this name!</span>
                                </p>
                                
                                <input type="password" id="pwd" name="password" placeholder="John" required></input>
                            </div>

                            <div className={` ${styles.signup_Form__EmailID} mb_24`}>
                                <p className="m_0 body_small__semibold text_3">Your email ID
                                    <span className="label_small__reg Primary_4">We’ll send you your child’s progress updates and invoices on this email ID</span>
                                </p>
                                
                                <input type="password" id="pwd" name="password" placeholder="johndoe@gmail.com" required></input>
                            </div>

                            <div className={` ${styles.signup_Form__EnterPass} mt_24 mb_24`}>
                                <p className="mt_0 body_small__semibold text_3">Create Password
                                    <span className="label_small__reg Primary_4">Your password will be required during login, and to access the parental admin section of the app, so make sure you set a password that is memorable! :)</span>
                                </p>

                                <input type="password" id="pwd" name="password" required></input>
                                <i className="fa-solid fa-eye" id="togglePWD"></i>
                                <i className="fa-solid fa-eye-slash hide" id="togglePWD"></i>
                            </div>

                            <button type="button" className={` ${styles.submit_btn} btn_Exlarge b_label__medium white`}>Submit</button>
                        </div>
                    </div>
                </div>
            </form>

            {/* footer section */}
            <footer className={styles.footerSection}>
                <ul>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Privacy Policy</a>
                    </li>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Terms of Service</a>
                    </li>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Help</a>
                    </li>
                </ul>
                <a href="#" className="text_center label_small__reg text_3">Google privacy policy and terms of service</a>
            </footer>
        </div>
    </div>
  )
}
