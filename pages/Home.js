import Head from 'next/head'
// import Navbar from '../components/Navbar';
import Image from 'next/image';
import Script from 'next/script';
import Header from '../components/Header';
import Footer from '../components/Footer';
import styles from '../styles/Home.module.scss';

// import Swiper core and required modules
import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

// import required modules
import { Keyboard, Navigation, Pagination } from "swiper";


export default function Home() {
    // const prev = () > {};
  return (
    <>  
        <Header></Header>
        <div className='Main_wrapper'>
            <div className={styles.userBooksSection}>
                <div className='c_container'>
                    <div className='c_row'>
                        <div className='col_12'>
                            <h1 className={` ${styles.userHeading} h_large__bold Primary_4`}>Welcome User!</h1>
                            <div className={` ${styles.banner}`}>
                                <div className={` ${styles.banner__box}`}>
                                    <Image
                                        src={"/banner1.png"}
                                        alt="Banner1"
                                        width={250}
                                        height={168}
                                    />
                                    <div className={styles.banner__headline}>
                                        <h6 className='body_large__semibold m_0 white'>
                                            <i className="icon icon-star"></i>New Releases!
                                        </h6>
                                    </div>
                                </div>
                                <div className={` ${styles.banner__box}`}>
                                    <Image
                                        src={"/banner2.png"}
                                        alt="Banner1"
                                        width={250}
                                        height={168}
                                    />
                                    <div className={styles.banner__headline}>
                                        <h6 className='body_large__semibold m_0 white'>
                                            <i className="icon icon-star"></i>Folktales from India
                                        </h6>
                                    </div>
                                </div>
                                <div className={` ${styles.banner__box}`}>
                                    <Image
                                        src={"/banner3.png"}
                                        alt="Banner1"
                                        width={250}
                                        height={168}
                                    />
                                    <div className={styles.banner__headline}>
                                        <h6 className='body_large__semibold m_0 white'>
                                            <i className="icon icon-star"></i>Stories from around the world!
                                        </h6>
                                    </div>
                                </div>
                                <div className={` ${styles.banner__box}`}>
                                    <Image
                                        src={"/banner4.png"}
                                        alt="Banner1"
                                        width={250}
                                        height={168}
                                    />
                                    <div className={styles.banner__headline}>
                                        <h6 className='body_large__semibold m_0 white'>
                                            <i className="icon icon-star"></i>Tales from Panchatantra
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='c_row'>
                        <div className='col_12'>
                            <div className={styles.contiRead}>
                                <h2 className={` ${styles.contiRead__Heading} h_small__bold Primary_4`}>Continue Reading
                                    
                                </h2>
                                <Image
                                    src={"/book_reading_icon.svg"}
                                    alt="continue reading"
                                    width={32}
                                    height={48}
                                />
                            </div>
                            
                            <div className={` ${styles.contiRead__banner}`}>
                                <div className={` ${styles.contiRead__bannerBox}`}>
                                    <Image
                                        src={"/StoryCover-Large-1.png"}
                                        alt="Banner1"
                                        width={282}
                                        height={206}
                                    />
                                    <div className={styles.info}>
                                        <a href='#'>
                                            <Image
                                                src={"/info_icon.svg"}
                                                alt="Information"
                                                width={32}
                                                height={32}
                                            /> 
                                        </a>
                                    </div>
                                </div>
                                <div className={` ${styles.contiRead__bannerBox}`}>
                                    <Image
                                        src={"/StoryCover-Large-2.png"}
                                        alt="Banner1"
                                        width={282}
                                        height={206}
                                    />
                                    <div className={styles.info}>
                                        <a href='#'>
                                            <Image
                                                src={"/info_icon.svg"}
                                                alt="Information"
                                                width={32}
                                                height={32}
                                            /> 
                                        </a>
                                    </div>
                                </div>
                                <div className={` ${styles.contiRead__bannerBox}`}>
                                    <Image
                                        src={"/StoryCover-Large-3.png"}
                                        alt="Banner1"
                                        width={282}
                                        height={206}
                                    />
                                    <div className={styles.info}>
                                        <a href='#'>
                                            <Image
                                                src={"/info_icon.svg"}
                                                alt="Information"
                                                width={32}
                                                height={32}
                                            /> 
                                        </a>
                                    </div>
                                </div>
                                <div className={` ${styles.contiRead__bannerBox}`}>
                                    <Image
                                        src={"/StoryCover-Large-4.png"}
                                        alt="Banner1"
                                        width={282}
                                        height={206}
                                    />
                                    <div className={styles.info}>
                                        <a href='#'>
                                            <Image
                                                src={"/info_icon.svg"}
                                                alt="Information"
                                                width={32}
                                                height={32}
                                            /> 
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className={styles.categorySection}>
                <div className='c_container'>
                    <div className='c_row'>
                        <div className='col_12'>
                            <div className={styles.category__header}>
                                <div className={styles.category__headerLeft}>
                                    <h4 className='h_small__bold Primary_6'>Stories by categories</h4>
                                </div>
                                <div className={styles.category__headerRight}>
                                    <ul>
                                        <li className='body_large__semibold Primary_4'>
                                            <a href='#'>
                                                <Image
                                                    src={"/Child.svg"}
                                                    alt="Age Selector"
                                                    width={32}
                                                    height={32}
                                                />
                                                All
                                            </a>
                                        </li>
                                        <li className='body_large__semibold Primary_4'>
                                            <a href='#'>
                                                <Image
                                                    src={"/Language.svg"}
                                                    alt="Language Selector"
                                                    width={32}
                                                    height={32}
                                                />
                                                All
                                            </a>
                                        </li>
                                        <li className={` ${styles.category__ViewAll} body_large__semibold Primary_4`}>
                                            <a href='#'>
                                                VIEW ALL CATEGORIES
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            {/* <div className='aa'> */}
                            
                            {/* </div> */}
                        </div>
                    </div>
                </div>

                <div className={styles.SwipersliderSection}>
                    <Swiper
                        slidesPerView={3}
                        spaceBetween={30}
                        centeredSlides={false}
                        slidesPerGroupSkip={1}
                        grabCursor={true}
                        keyboard={{
                          enabled: true,
                        }}
                        breakpoints={{
                            640: {
                                slidesPerView: 2,
                                spaceBetween: 20,
                            },
                            768: {
                                slidesPerView: 3,
                                spaceBetween: 40,
                            },
                            1024: {
                                slidesPerView: 4,
                                spaceBetween: 24,
                            },
                        }}
                        scrollbar={true}
                        navigation={true}
                        // pagination={{
                        //   clickable: true,
                        // }}
                        modules={[Keyboard, Navigation, Pagination]}
                        className={styles.mySwiper}
                    >
                        <SwiperSlide className={styles.mySwiper__slide}>
                            <Image
                                src={"/slider1.png"}
                                alt="Slider1"
                                width={282}
                                height={206}
                                className={styles.mySwiper__slideImg}
                            />
                            <div className={styles.mySwiper__info}>
                                <Image
                                    src={"/slider1.png"}
                                    alt="Slider1"
                                    width={368}
                                    height={234}
                                    className={styles.mySwiper__infoDetailsImg}
                                />
                                <button type='button' className='btn_large body_large__bold white'>Start Reading!</button>
                                <p className='body_small__reg'>
                                Anansi the spider is known for his 8 legs. But, how did they get so long and thin? Let’s journey through the jungle with Anansi and his friends to find out, in this African folktale.
                                </p>
                                <a href='#' className='body_large__bold Primary_4'>More details</a>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide className={styles.mySwiper__slide}>
                            <Image
                                src={"/slider2.png"}
                                alt="Slider1"
                                width={282}
                                height={206}
                                className={styles.mySwiper__slideImg}
                            />
                            <div className={styles.mySwiper__info}>
                                <h4>aaaaaaaaaaaaaa</h4>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide className={styles.mySwiper__slide}>
                            <Image
                                src={"/slider3.png"}
                                alt="Slider1"
                                width={282}
                                height={206}
                                className={styles.mySwiper__slideImg}
                            />
                            <div className={styles.mySwiper__info}>
                                <h4>aaaaaaaaaaaaaa</h4>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide className={styles.mySwiper__slide}>
                            <Image
                                src={"/slider4.png"}
                                alt="Slider1"
                                width={282}
                                height={206}
                                className={styles.mySwiper__slideImg}
                            />
                            <div className={styles.mySwiper__info}>
                                <h4>aaaaaaaaaaaaaa</h4>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide className={styles.mySwiper__slide}>
                            <Image
                                src={"/slider5.png"}
                                alt="Slider1"
                                width={282}
                                height={206}
                                className={styles.mySwiper__slideImg}
                            />
                            <div className={styles.mySwiper__info}>
                                <h4>aaaaaaaaaaaaaa</h4>
                            </div>
                        </SwiperSlide>
                        <SwiperSlide className={styles.mySwiper__slide}>
                            <Image
                                src={"/slider6.png"}
                                alt="Slider1"
                                width={282}
                                height={206}
                                className={styles.mySwiper__slideImg}
                            />
                        </SwiperSlide>
                        <SwiperSlide className={styles.mySwiper__slide}>
                            <Image
                                src={"/slider7.png"}
                                alt="Slider1"
                                width={282}
                                height={206}
                                className={styles.mySwiper__slideImg}
                            />
                        </SwiperSlide>
                        <SwiperSlide className={styles.mySwiper__slide}>
                            <Image
                                src={"/slider8.png"}
                                alt="Slider1"
                                width={282}
                                height={206}
                                className={styles.mySwiper__slideImg}
                            />
                        </SwiperSlide>
                        <SwiperSlide className={styles.mySwiper__slide}>
                            <Image
                                src={"/slider9.png"}
                                alt="Slider1"
                                width={282}
                                height={206}
                                className={styles.mySwiper__slideImg}
                            />
                        </SwiperSlide>
                        
                    </Swiper>
                </div>
            </div>
        </div>
        <Footer></Footer>

        
    </>
  )
}
