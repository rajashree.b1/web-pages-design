// import Navbar from "../components/Navbar";
// import { SassColor } from "sass";

import styles from "../styles/Login.module.scss";
import Image from 'next/image';
// import MyImg from "../public/logo";

export default function SignUpEmail_confirmMailSent(){
  return(
    <div>
        <div className={styles.login_wrapper}>

            {/* login header */}
            <header className={styles.logo}>
                <a href="https://www.jumbaya.com/" target="_blank">
                    
                    <Image
                        src={"/logo.svg"}
                        alt="Logo"
                        width={"100%"}
                        height={61}
                    />
                </a>
            </header>

            {/* main content section */}
            <form role="form" id={styles.FormWrapper} className={styles.FormWrapper}>
                <div className={styles.Form__content}>
                    <h4 className="text_center body_large__semibold text_3 mt_40 mb_40">Signing Up with Email</h4>
                    <h1 className="text_center h_large__reg Primary_4 mt_0 mb_16">We’ve sent you a confirmation mail on johndoe@gmail.com

                    <p className="text_center body_small__reg text_3">Please follow the steps mentioned there to finish creating your account!</p>
                    <br></br>
                    <Image
                        src={"/smily.svg"}
                        alt="Smily"
                        width={50}
                        height={50}
                    />
                    </h1>
                    <p className="text_center body_small__reg text_3">Did not receive our email? Please check your spam folder.  If you still don’t see it, make sure you submitted the correct email ID.</p>
                    <div className={styles.SignUp__editEmailLink}>
                        <a href="#" className="text_center body_large__bold Primary_4">Edit Email</a>
                    </div>
                </div>
            </form>

            {/* footer section */}
            <footer className={styles.footerSection}>
                <ul>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Privacy Policy</a>
                    </li>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Terms of Service</a>
                    </li>
                    <li>
                    <a href="#" className="body_small__semibold text_3">Help</a>
                    </li>
                </ul>
                <a href="#" className="text_center label_small__reg text_3">Google privacy policy and terms of service</a>
            </footer>
        </div>
    </div>
  )
}
  