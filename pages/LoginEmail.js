// import Navbar from "../components/Navbar";
// import { SassColor } from "sass";

import styles from "../styles/Login.module.scss";
import Image from 'next/image';
// import MyImg from "../public/logo";

export default function LoginEmail(){
  return(
    <div>
        
        <div className={styles.login_wrapper}>

            {/* header section */}
            <header className={styles.logo}>
                <a href="https://www.jumbaya.com/" target="_blank">
                    
                    <Image
                        src={"/logo.svg"}
                        alt="Logo"
                        width={"100%"}
                        height={61}
                    />
                </a>
            </header>

            {/* main content section */}
            <form role="form" id={styles.FormWrapper} className={styles.FormWrapper}>
                <div className={styles.Form__content}>
                    <h4 className="text_center body_large__semibold text_3 mt_40 mb_16">Login using email</h4>
                    <div className={` ${styles.Form__EmailSection} Bgwhite box_shadow1`}>
                        <p className="mt_0 body_small__semibold text_3">Enter Email ID</p>
                        <div className={styles.Form__EnterEmail}>
                            <input type="text" id="email" name="email" placeholder="johndoe@gmail.com" required></input>
                        </div>

                        <button type="button" className={` ${styles.otp_btn} mt_24 mb_24 btn_Exlarge b_label__medium white`}>Get OTP</button>
                        <button type="button" className={` ${styles.loginPass_btn} second_ExlargeOutlined b_label__medium `}>Login using password</button>
                    </div>

                    <div className={styles.signUp_link}>
                        <span className="body_small__reg Black">Don’t have an account?</span>
                        <a href="#" className="b_label__bold Primary_4">sign up</a>
                    </div>
                </div>
            </form>

            {/* footer section */}
            <footer className={styles.footerSection}>
            <ul>
                <li>
                <a href="#" className="body_small__semibold text_3">Privacy Policy</a>
                </li>
                <li>
                <a href="#" className="body_small__semibold text_3">Terms of Service</a>
                </li>
                <li>
                <a href="#" className="body_small__semibold text_3">Help</a>
                </li>
            </ul>
            <a href="#" className="text_center label_small__reg text_3">Google privacy policy and terms of service</a>
            </footer>
        </div>
    </div>
  )
}
  