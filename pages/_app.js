import '../styles/globals.scss'
import "../styles/_colors.scss";
import "../styles/_mixins.scss";
import "../styles/_variables.scss";
import "../styles/iconfonts.css";
// import "../components/BtnComponent.js";
// import "../js/custom.js";

function MyApp({ Component, pageProps }) {
  return (
    <Component {...pageProps} />
  )
  // <div className='hello'>Hello World</div>
}

export default MyApp;