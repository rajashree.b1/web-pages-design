// import Navbar from "../components/Navbar";
// import { SassColor } from "sass";

import styles from "../styles/Login.module.scss";
import Image from 'next/image';
// import { StyledLargeBtn, LargeOutlineBtn } from "../components/BtnComponent";
// import { DSmall, HeadSmallReg, BodySmallS_Bold, Input, Validation, LinkSemiBold, LinkReg, BodySmallReg } from "../components/TypoComponent";

export default function Login({ Component, pageProps }){
  return(
    <div>
        
        <div className={styles.login_wrapper}>

            {/* login header */}
            <header className={styles.logo}>
                <a href="https://www.jumbaya.com/" target="_blank">
                    <Image
                        src={"/logo.svg"}
                        alt="Logo"
                        width={"100%"}
                        height={61}
                    />
                </a>
            </header>

            {/* login main content */}
            <form role="form" id={styles.loginForm} name="loginForm">
                <div className={styles.login_formContent}>
                    <h1 className="text_center h_small__reg Primary_4 mt_40 mb_16">Welcome to Jumbaya!</h1>
                    {/* <HeadSmallReg className="text_center Primary_4 mt_40 mb_16">Welcome to Jumbaya!</HeadSmallReg> */}
                    <div className={`${styles.login_formNumbers} Bgwhite box_shadow1`}>
                        <p className="mt_0 body_small__semibold text_3">Mobile Number</p>
                        {/* <BodySmallS_Bold className="text_3 mb_8">Mobile Number</BodySmallS_Bold> */}
                        <div className={styles.countrySection}>
                            <select id={styles.country} className="body_small__reg">
                                <option value="select country">&#x1F1EE;&#x1F1F3; +91</option>
                                <option value="andorra">&#x1F1E6;&#x1F1E9; +1</option>
                                <option value="ai">&#x1F1E6;&#x1F1E8; +44</option>
                                <option value="am">&#x1F1E6;&#x1F1F2; +55</option>
                                <option value="india">&#x1F1EE;&#x1F1F3; +91</option>
                            </select>

                            <input type="tel" id={styles.output} placeholder="10 digit number" className="body_small__reg" />
                            {/* <Input type="tel" id={styles.output} placeholder="10 digit number"></Input> */}
                        </div>
                    
                        <span id="valid_msg" className={` ${styles.valid_msg} hide`}>Valid</span>
                        <span id="invalid_msg" className={` ${styles.invalid_msg} hide`}>Invalid number</span>
                        {/* <Validation id="valid-msg" className="hide">Valid Number</Validation>
                        <Validation id="error-msg" className="hide">Invalid Number</Validation> */}
                    
                        {/* <StyledLargeBtn type="button" className={` ${styles.otp_btn} mb_24 mt_24 white`}>Get OTP</StyledLargeBtn>
                        <LargeOutlineBtn type="button" className={` ${styles.loginPass_btn} capitalize`}>Login using password</LargeOutlineBtn> */}

                        <button type="button" className={` ${styles.otp_btn} mb_24 mt_24 btn_large b_label__medium white`}>Get OTP</button>
                        
                        <button type="button" className={` ${styles.loginPass_btn} second_LargeOutlined b_label__medium capitalize`}>Login using password</button>
                    </div>
                    <div className={` ${styles.login_formGoogle} Bgwhite  box_shadow1`}>
                        {/* <LinkSemiBold href="#" className="text_4">Continue with Google</LinkSemiBold> */}
                        <a href="#" className="body_small__semibold text_4">Continue with Google</a>

                    </div>

                    <div className={` ${styles.login_formEmail} Bgwhite box_shadow1`}>
                        {/* <LinkSemiBold href="#" className="text_4">Continue with Email</LinkSemiBold> */}
                        <a href="#" className="body_small__semibold text_4">Continue with Email</a>
                    </div>

                    <div className={styles.signUp_link}>
                        <span className="body_small__reg black">Don’t have an account?</span>
                        <a href="#" className="b_label__bold Primary_4">sign up</a>
                        {/* <BodySmallReg className="black">Don’t have an account?</BodySmallReg>
                        <LinkSemiBold href="#" className="Primary_4">sign up</LinkSemiBold> */}
                        
                    </div>
                </div>
            </form>

            {/* login footer */}
            <footer className={styles.login_formFooter}>
            <ul>
                <li>
                    <a href="#" className="body_small__semibold text_3">Privacy Policy</a>
                    {/* <LinkSemiBold href="#" className="text_3">Privacy Policy</LinkSemiBold> */}
                </li>
                <li>
                    <a href="#" className="body_small__semibold text_3">Terms of Service</a>
                    {/* <LinkSemiBold href="#" className="text_3">Terms of Service</LinkSemiBold> */}
                </li>
                <li>
                    <a href="#" className="body_small__semibold text_3">Help</a>
                    {/* <LinkSemiBold href="#" className="text_3">Help</LinkSemiBold> */}
                </li>
            </ul>
            <a href="#" className="text_center label_small__reg text_3">Google privacy policy and terms of service</a>
            {/* <LinkReg href="#" className={` ${styles.google_privacy} text_center text_3`}>Google privacy policy and terms of servic</LinkReg> */}

            </footer>
        </div>
    </div>
  )
}
  