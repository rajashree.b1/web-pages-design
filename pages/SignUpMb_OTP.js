// import Navbar from "../components/Navbar";
// import { SassColor } from "sass";

import styles from "../styles/Login.module.scss";
import Image from 'next/image';
// import MyImg from "../public/logo";

export default function SignUpMb_OTP(){
  return(
    <div>
        
        <div className={styles.login_wrapper}>

            {/* login header */}
            <header className={styles.logo}>
                <a href="https://www.jumbaya.com/" target="_blank">
                    
                    <Image
                        src={"/logo.svg"}
                        alt="Logo"
                        width={"100%"}
                        height={61}
                    />
                </a>
            </header>


            {/* main content section */}
            <form role="form" id={styles.FormWrapper} className={styles.FormWrapper}>
                <div className={styles.Form__content}>
                    <h4 className="text_center body_large__semibold text_3 mt_40 mb_16">Signing Up with Mobile Number</h4>
                    <div className={` ${styles.Form__MobNumbers} Bgwhite  box_shadow1`}>
                        <p className="mt_0 body_small__semibold text_3">OTP Sent on</p>

                        <div className={styles.Form__edit_mbSection}>
                            <input className={` ${styles.mb_no} Primary_4`} value="+91 1234567890" size="10" />
                            <a href="#">
                                <Image
                                    src={"/Edit.svg"}
                                    alt="Edit"
                                    width={24}
                                    height={24}
                                />
                            </a>
                        </div>
                        <div className={styles.Form__EnterOTPSection}>
                            <p className="mt_0 body_small__semibold text_3">Enter OTP</p>
                            <input type="text" id="digit_1" name="digit_1" />
                            <input type="text" id="digit_2" name="digit_2" />
                            <input type="text" id="digit_3" name="digit_3" />
                            <input type="text" id="digit_4" name="digit_4" />
                            <input type="text" id="digit_5" name="digit_5" />
                            <input type="text" className="mr_0" id="digit_6" name="digit_6" />

                            <div className={styles.Form__OTPTimer}>
                                <span className="text_center body_large__semibold Primary_4">00:20</span>
                            </div>
                        </div>

                        <button type="button" className={` ${styles.submit_btn} btn_Exlarge b_label__medium white`}>Submit</button>
                    </div>

                    <div className={styles.signUp_link}>
                        <span className="body_small__reg Black">Already have an account?</span>
                        <a href="#" className="b_label__bold Primary_4">login</a>
                    </div>
                </div>
            </form>

            {/* footer section */}
            <footer className={styles.footerSection}>
            <ul>
                <li>
                <a href="#" className="body_small__semibold text_3">Privacy Policy</a>
                </li>
                <li>
                <a href="#" className="body_small__semibold text_3">Terms of Service</a>
                </li>
                <li>
                <a href="#" className="body_small__semibold text_3">Help</a>
                </li>
            </ul>
            <a href="#" className="text_center label_small__reg text_3">Google privacy policy and terms of service</a>
            </footer>
        </div>
    </div>
  )
}
  