import Document, {Html, Head, Main, NextScript} from 'next/document';

class MyDocument extends Document{
    render(){
        return(
            <Html>
                <Head>
                    <link href='https://fonts.googleapis.com/css2?family=Baloo+2:wght@400;500;600;700;800&display=swap'
                    rel='stylesheet' />

                    <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css' rel='stylesheet' />

                    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap" rel="stylesheet" />

                    
                </Head>

                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
            
        )
    }
}

export default MyDocument;