import Link from "next/link";
// why we used link compnet bcoz page will not refresh without link component page it should be refresh after click menu

const Navbar = () => {
  return (
    <div className="Menus">
        <nav className="Login_menu">
            <ul className="menu_bar">
                <li>
                    <Link href="/Login">
                    <a>Login</a>
                    </Link>
                </li>
                <li>
                    <Link href="/LoginMb_OTP">
                    <a>Login Mobile OTP</a>
                    </Link>
                </li>
                <li>
                    <Link href="/LoginMb_Pass">
                    <a>Login Mobile Password</a>
                    </Link>
                </li>
                <li>
                    <Link href="/LoginEmail">
                    <a>Login Email</a>
                    </Link>
                </li>
                <li>
                    <Link href="/LoginEmail_OTP">
                    <a>Login Email OTP</a>
                    </Link>
                </li>
                <li>
                    <Link href="/LoginEmail_Pass">
                    <a>Login Email Pass</a>
                    </Link>
                </li>
                <li>
                    <Link href="/LoginGoogle">
                    <a>Login Google</a>
                    </Link>
                </li>
                <li>
                    <Link href="/LoginEmail_ForgotPass">
                    <a>Email Forgot Password</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SentForgot_Pass">
                    <a>Link to Reset your Password</a>
                    </Link>
                </li>
                <li>
                    <Link href="/LoginEmail_ResetPass">
                    <a>Reset Password</a>
                    </Link>
                </li>
            </ul>
        </nav>

        {/* signup menu */}
        <nav className="Signup_menu">
            <ul className="menu_bar">
                <li>
                    <Link href="/SignUp">
                    <a>Sign Up</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpMb_OTP">
                    <a>SignUp Mb OTP</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpMb_verify">
                    <a>SignUpMb_verify</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpMb_Form">
                    <a>SignUp Mobile Form</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpEmail_Sent">
                    <a>SignUpEmail_Sent</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUp_AccComplt">
                    <a>SignUp Account Complete</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpEmail">
                    <a>SignUp Email</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpEmail_confirmMailSent">
                    <a>SignUpEmail confirmation Mail Sent</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpEmail_verify">
                    <a>SignUpEmail verify</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpEmail_Form">
                    <a>SignUp Email Form</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpGoogle">
                    <a>SignUp Google</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpGoogleForm">
                    <a>SignUp Google Form</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpMb_OTPVerify">
                    <a>SignUpMb_OTPVerify</a>
                    </Link>
                </li>
                <li>
                    <Link href="/SignUpMb_OTPVerified">
                    <a>SignUpMb_OTPVerified</a>
                    </Link>
                </li>
               
            </ul>
        </nav>

        {/* home screens */}
        <nav className="main_menu">
            <ul>
                <li>
                    <Link href="/Home">
                        <a target="_blank">Home</a>
                    </Link>
                </li>
            </ul>
        </nav>
    </div>
  )
}

export default Navbar