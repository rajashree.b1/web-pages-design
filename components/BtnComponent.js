import styled from 'styled-components';


export const StyledLargeBtn = styled.button`
    width: 368px;
    height: 48px;
    font-family: "Baloo 2";
    font-size: 14px;
    line-height: 24px;
    font-weight: 500;
    background: #953389;
    box-shadow: 0px 4px 0px rgba(0, 0, 0, 0.12), inset 0px -4px 0px rgba(0, 0, 0, 0.25);
    border-radius: 16px;
    border: 0;
    &:active{
        background-color: #6E2665;
    }
    &:hover{
        background-color: #ffffff;
        border: 2px solid #953389;
        color: #953389;
        box-shadow: none;
    }
`;

export const LargeOutlineBtn= styled.button`
    width: 368px;
    height: 48px;
    padding: 12px 0;
    font-family: "Baloo 2";
    font-size: 14px;
    line-height: 24px;
    font-weight: 500;
    background-color: #ffffff;
    box-shadow: none;
    border-radius: 16px;
    border: 2px solid #953389;
    color: #953389;
`;




