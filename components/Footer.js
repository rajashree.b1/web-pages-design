import Link from "next/link";
import Image from "next/image";

import styles from '../styles/Footer.module.scss';

export default function Footer(){
    return(
        <>
            <footer>
                <div className="c_container">
                    <div className="c_row">
                        <div className="col_12">
                            <div className={styles.footerContent}>
                                <h6 className="h_large__bold m_0 Primary_4">All good stories must come to an end! :(</h6>
                                <p className="body_large__reg black">You have scrolled to the bottom of our catalog! We’re working hard to make more stories <br></br>available to you as soon as possible! Stay tuned! :)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    )
}