import styled from 'styled-components';

export const DLarge = styled.div`
    font-family: "Baloo 2";
    font-weight: 400;
    font-size: 56px;
    line-height: 84px;
`;

export const DSmall = styled.div`
    font-size: 40px;
    line-height: 60px;
    font-family: "Baloo 2";
    font-weight: 400;
`;

export const HeadLargeBold = styled.div`
    font-size: 32px;
    line-height: 48px;
    font-family: "Baloo 2";
    font-weight: 700;
`;


export const HeadLargeReg = styled.div`
    font-size: 32px;
    line-height: 48px;
    font-family: "Baloo 2";
    font-weight: 400;
`;

export const HeadSmallBold = styled.div`
    font-size: 24px;
    line-height: 36px;
    font-family: "Baloo 2";
    font-weight: 700;
`;

export const HeadSmallReg = styled.div`
    font-size: 24px;
    line-height: 36px;
    font-family: "Baloo 2";
    font-weight: 400;
`;

export const SubHSemiBold= styled.div`
    font-size: 20px;
    line-height: 30px;
    font-family: "Baloo 2";
    font-weight: 600;
`;

export const SubHSemiReg= styled.div`
    font-size: 20px;
    line-height: 30px;
    font-family: "Baloo 2";
    font-weight: 400;
`;

export const BodyLrgeS_Bold= styled.div`
    font-size: 16px;
    line-height: 24px;
    font-family: "Baloo 2";
    font-weight: 600;
`;

export const BodyLrgeReg= styled.div`
    font-size: 16px;
    line-height: 24px;
    font-family: "Baloo 2";
    font-weight: 400;
`;

export const BodySmallS_Bold= styled.div`
    font-size: 14px;
    line-height: 24px;
    font-family: "Baloo 2";
    font-weight: 700;
`;

export const BodySmallReg= styled.div`
    font-size: 14px;
    line-height: 24px;
    font-family: "Baloo 2";
    font-weight: 400;
`;

export const Input= styled.input`
    font-size: 14px;
    line-height: 24px;
    font-family: "Baloo 2";
    font-weight: 400;
`;

export const LabelLargeS_Bold= styled.div`
    font-size: 12px;
    line-height: 18px;
    font-family: "Baloo 2";
    font-weight: 600;
`;

export const LabelLargeReg= styled.div`
    font-size: 12px;
    line-height: 18px;
    font-family: "Baloo 2";
    font-weight: 400;
`;

export const LabelSmallS_Bold= styled.div`
    font-size: 10px;
    line-height: 15px;
    font-family: "Baloo 2";
    font-weight: 600;
`;

export const LabelSmallReg= styled.div`
    font-size: 10px;
    line-height: 15px;
    font-family: "Baloo 2";
    font-weight: 400;
`;

export const Validation= styled.span`
    font-size: 12px;
    line-height: 15px;
    font-family: "Baloo 2";
    font-weight: 400;
    color: #ff0000;
`;

export const LabelMedium= styled.div`
    font-size: 14px;
    line-height: 24px;
    font-family: "Baloo 2";
    font-weight: 500;
`;

export const LabelBold= styled.div`
    font-size: 14px;
    line-height: 24px;
    font-family: "Baloo 2";
    font-weight: 700;
`;

export const LinkSemiBold = styled.a`
    font-size: 14px;
    line-height: 24px;
    font-family: "Baloo 2";
    font-weight: 600;
`;

export const LinkReg = styled.a`
    font-size: 10px;
    line-height: 15px;
    font-family: "Baloo 2";
    font-weight: 400;
`;

// export const BodySmallReg= styled.div`
//     font-size: 14px;
//     line-height: 24px;
//     font-family: "Baloo 2";
//     font-weight: 400;
// `;