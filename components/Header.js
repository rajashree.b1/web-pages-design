import Link from "next/link";
import Image from "next/image";

import styles from '../styles/Header.module.scss';

export default function Header(){
    return(
        <>
            <header className={styles.header}>
                <div className="c_container">
                    <div className="c_row">
                        <div className="col_12">
                            <div className={styles.header__content}>
                                {/* logo section */}
                                {/* <div className={styles.logoSection}> */}
                                    <a href="https://www.jumbaya.com/" target="_blank">
                                        <Image
                                            src={"/logo.svg"}
                                            alt="Logo"
                                            width={"100%"}
                                            height={61}
                                        />
                                    </a>
                                {/* </div> */}

                                <div className={styles.navbar_menu}>
                                    <ul>
                                        <li className={styles.home_icon}>
                                            <Link href="/Home">
                                                <a>
                                                    <i className="icon icon-home"></i>
                                                </a>
                                            </Link>
                                        </li>

                                        <li className={styles.book_icon}>
                                            <Link href="/Book">
                                                <a>
                                                    <i className="icon icon-book"></i>
                                                </a>
                                            </Link>
                                        </li>

                                        <li className={styles.search_icon}>
                                            <Link href="/Search">
                                                <a>
                                                    <i className="icon icon-search"></i>
                                                </a>
                                            </Link>
                                        </li>

                                        <li className={styles.noti_icon}>
                                            <Link href="/Notification">
                                                <a>
                                                    <i className="icon icon-notification"></i>
                                                    <span className={styles.noti_counter}>12</span>
                                                </a>
                                            </Link>
                                        </li>
                                        
                                        <li className={styles.avtar}>
                                            <Link href="/Avtar">
                                                <a>
                                                    <Image
                                                        src={"/Avatar_img.png"}
                                                        alt="Avtar"
                                                        width={48}
                                                        height={48}
                                                    />
                                                </a>
                                            </Link>
                                        </li>
                                
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </>
    )
}