/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
}

module.exports = nextConfig

module.exports = {
  devtool: "source-map",

  compiler: {
    // ssr and displayName are configured by default
    styledComponents: true,
  },
}



